
import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import firebase from '../database/firebase';


export default function LoginPage(){

    return(
    <View style={styles.mainContainer}>
      <View style={styles.appName}>
        <Text style={styles.appText}>APPKU</Text>
      </View>
      <View style={styles.inputContainer}>
        <View style={styles.inputIDContainer}>
          <TextInput style={styles.inputID} placeholder="Email"/>
        </View>
        <View style={styles.inputPassContainer}>
          <TextInput style={styles.inputPass} placeholder="Password" secureTextEntry={true}/>
        </View>
        <TouchableOpacity style={styles.forgotText}>
            <Text>Forget Password ?</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.buttonContainer}>
        <View style={styles.buttonLogin}>
          <TouchableOpacity style={styles.buttonLoginPage}>
            <Text style={styles.loginText}>LOGIN</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonLoginPage}>
            <Text style={styles.loginGoogleText}>Login with Google</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
    );
}


const styles = StyleSheet.create({
    mainContainer : {
      width : '100%',
      height : '100%'
    },
    appName : {
      marginLeft : '8%',
      marginRight : '8%',
      marginTop : '8%',
      justifyContent : 'center',
      alignItems : 'center'
    },
    appText : {
      fontSize : 80,
      color : 'black'
    },
    forgotText : {
      alignSelf : 'flex-end',
      marginTop : '1%'
    },
    inputContainer : {
      marginLeft : '8%',
      marginRight : '8%',
      marginTop : '8%',
      justifyContent : 'center',
      alignItems : 'center',
    },
    inputIDContainer : {
      width : '100%',
      marginTop : '10%',
      borderWidth : 2
    },
    inputPassContainer :{
      marginTop : '5%',
      width : '100%',
      borderWidth : 2
    },
    buttonContainer : {
      marginTop: '15%'
    },  
    buttonLoginPage : {
      marginLeft : '8%',
      marginRight : '8%',
      backgroundColor : 'green',
      justifyContent : 'center',
      alignItems : 'center',
      padding : '2%',
      marginTop : '2%'
    },
    loginText : {
      color : 'white',
      fontSize : 18
    },
    loginGoogleText : {
      color : 'white'
    }
    
    });