import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyA-9VRAaX614M7OvPoF_vX1WuHDHrX9AmU",
    authDomain: "challenge06-90bb4.firebaseapp.com",
    projectId: "challenge06-90bb4",
    storageBucket: "challenge06-90bb4.appspot.com",
    messagingSenderId: "439256718006",
    appId: "1:439256718006:web:ac44daf56b0a6cf0776417",
    measurementId: "G-812CCBRDLP"
};

firebase.initializeApp(firebaseConfig);
export default firebase